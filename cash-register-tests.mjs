import { test, run } from  './test.mjs';
import CashRegister from './cash-register.mjs';
import Item from './item.mjs';
import { strict as assert } from 'assert';
import { ArgumentException } from './exceptions.mjs';

test("CashRegister can start new order", () => {
    const register = new CashRegister();

    assert.doesNotThrow(() => register.newOrder());
});

test("New orders have empty list of items", () => {
    let expected, actual;
    const register = new CashRegister();

    register.newOrder();

    expected = 0;
    actual = register.getOrderItems().length;

    assert.equal(actual, expected);

    register.newOrder();

    expected = 0;
    actual = register.getOrderItems().length;

    assert.equal(actual, expected);
});

test("New orders receive the next available order number", () => {
    let expected, actual;
    
    const register = new CashRegister();
    register.newOrder();

    expected = 1;
    actual = register.getOrderNumber();

    assert.equal(actual, expected);

    register.newOrder();
    
    expected = 2;
    actual = register.getOrderNumber();

    assert.equal(actual, expected);
});

test("CashRegister can add an item to the current order", () => {
    const register = new CashRegister();

    register.newOrder();

    const item = new Item("test item", 2.99);
    assert.doesNotThrow(() => register.addItem(item));
});

test("CashRegister throws error if you try to add an incorrectly created item", () => {
    const register = new CashRegister();

    register.newOrder();

    const item = {
        derp: "derp",
    };

    assert.throws(() => register.addItem(item));

    try {
        register.addItem(item);
    } catch(e) {
        assert.equal(e instanceof ArgumentException, true);
    }
});

test("CashRegister throws error if you try to add no item", () => {
    const register = new CashRegister();

    register.newOrder();

    assert.throws(() => register.addItem());
    assert.throws(() => register.addItem(null));
});

test("CashRegister returns subtotal of all added items", () => {
    const register = new CashRegister();

    register.newOrder();
    register.addItem(new Item("apple", 1.99)).addItem(new Item("banana", 1.99));

    const expected = 3.98;
    const actual = register.getSubtotal();

    assert.equal(actual, expected);
});

test("CashRegister cannot create new order if current order has unpaid balance", () => {
    const register = new CashRegister();

    register.newOrder();
    register.addItem(new Item("apple", 1.99));

    assert.throws(() => register.newOrder());

    try {
        register.newOrder();
    } catch (err) {
        assert.equal(err.message, "Cannot create a new order until current order is paid to zero balance.")
    }
});

test("CashRegister can add payment", () => {
    const register = new CashRegister();

    register.newOrder();
    register.addPayment(3.00);

    assert.equal(register.getBalance(), -3.00);
});

run();
