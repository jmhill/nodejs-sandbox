import Item from './item.mjs';
import { ArgumentException, InvalidOperationException } from './exceptions.mjs';

export default class CashRegister {

    #orderItems = [];
    #orderNumber = 0;
    #paid = 0;

    newOrder() {
        if (this.getBalance() !== 0) {
            throw new InvalidOperationException("Cannot create a new order until current order is paid to zero balance.");
        }
        this.#orderNumber++;
        return this;
    }

    getBalance() {
        const payments = this.#paid;
        const due = this.getTotal();
        return due - payments;
    }

    getOrderItems() {
        return this.#orderItems.slice();
    }

    getOrderNumber() {
        return this.#orderNumber;
    }

    addItem(item) {
        if (!(item instanceof Item)) {
            throw new ArgumentException("Attempted to add invalid Item to order");
        }
        this.#orderItems.push(item);
        return this;
    }

    addPayment(amount) {
        this.#paid += amount;
        return this;
    }

    getSubtotal() {
        const reducer = (previous, current) => previous + current.price();
        return this.#orderItems.reduce(reducer, 0);
    }

    getTotal() {
        const total = this.getSubtotal();
        return total;
    }
}