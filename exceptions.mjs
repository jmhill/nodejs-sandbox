
export class ArgumentException extends Error {}

export class InvalidOperationException extends Error {}
