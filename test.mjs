let tests = [];

export function test(name, fn) {
    tests.push({name, fn});
}

export function run() {
    tests.forEach(t => {
        try {
            t.fn();
            console.log(`PASS: ${t.name}`);
        } catch (err) {
            console.log(`FAIL: ${t.name}`);
            console.log(err.stack);
        }
    });
}