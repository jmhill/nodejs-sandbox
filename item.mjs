export default class Item {
    
    #name;
    #price;

    constructor(name, price) {
        this.#name = name;
        this.#price = price;
    }

    name() {
        return this.#name;
    }

    price() {
        return this.#price;
    }
}