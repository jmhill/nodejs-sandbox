import { test, run } from './test.mjs';
import assert from 'assert';
import Calculator from './calculator.mjs';
import { ArgumentException } from './exceptions.mjs';

test("Test tool accurately reports PASS on true === true", () => {
    assert.ok(true === true);
});

test("calculator can add one number", () => {
    const expected = 2;

    const myCalculator = new Calculator();
    myCalculator.add(2);

    const actual = myCalculator.getResult();

    assert.strictEqual(actual, expected);
});

test("calculator can add multiple numbers", () => {
    const expected = 5;

    const myCalculator = new Calculator();
    
    myCalculator.add(2).add(3);
    
    const actual = myCalculator.getResult();

    assert.strictEqual(actual, expected);
});

test("calculator can add and subtract numbers", () => {
    const expected = 3;

    const myCalculator = new Calculator();
    myCalculator.add(5).subtract(2);

    const actual = myCalculator.getResult();
    
    assert.strictEqual(actual, expected);
});

test("calculator can multiply numbers", () => {
    const expected = 25;

    const myCalculator = new Calculator();

    myCalculator.add(5).multiplyBy(5);

    const actual = myCalculator.getResult();

    assert.strictEqual(actual, expected);
});

test("calculator can divide numbers", () => {
    const expected = 2;

    const myCalculator = new Calculator();

    myCalculator.add(8).divideBy(4);
    const actual = myCalculator.getResult();

    assert.strictEqual(actual, expected);
});

test("calculator throws when attempting to divide by zero", () => {
    const myCalculator = new Calculator();

    assert.throws(() => myCalculator.add(2).divideBy(0).getResult());
});

test("calculator throws when attempting to add a non-number input", () => {
    const myCalculator = new Calculator();

    const badOperation = () => myCalculator.add(2).add("derp").getResult();
    
    assert.throws(badOperation);
    checkForArgumentException(badOperation);
});

test("calculator throws when attempting to subtract a non-number input", () => {
    const myCalculator = new Calculator();

    const badOperation = () => myCalculator.add(2).add("derp").getResult();
    
    assert.throws(badOperation);
    checkForArgumentException(badOperation);
});

test("calculator throws when attempting to multipy a non-number input", () => {
    const myCalculator = new Calculator();

    const badOperation = () => myCalculator.add(2).add("derp").getResult();
    
    assert.throws(badOperation);
    checkForArgumentException(badOperation);
});

test("calculator throws when attempting to divide a non-number input", () => {
    const myCalculator = new Calculator();

    const badOperation = () => myCalculator.add(2).add("derp").getResult();
    
    assert.throws(badOperation);
    checkForArgumentException(badOperation);
});

const checkForArgumentException = (fn) => {
    try {
        fn();
    } catch(e) {
        assert.ok(e instanceof ArgumentException, `expected error to be ArgumentException`);
    }
}

run();