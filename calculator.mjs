import { ArgumentException } from './exceptions.mjs';

export default class Calculator {
    #result = 0;

    add(number) {
        guard(number);
        this.#result += number;
        return this;
    }

    subtract(number) {
        this.#result -= number;
        return this;
    }

    multiplyBy(number) {
        this.#result *= number;
        return this;
    }

    divideBy(number) {
        if (number === 0) {
            throw new ArgumentException("Cannot divide by 0");
        }
        this.#result /= number;
        return this;
    }

    getResult() {
        return this.#result;
    }
}

const isNumber = (input) => typeof(input) === 'number';

const guard = (input) => {
    if (!isNumber(input)) {
        throw new ArgumentException(`Calculator only operates on number inputs. You provided argument: "${input}" of type "${typeof input}".`);
    }
}
